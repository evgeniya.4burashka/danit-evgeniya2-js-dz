/** 1. Метод об'єкту це функція в середині об'єкту.
 *  2. Значення властивостей об'єкта можуть бути будь-якого типу даних.
 *  3. Значенням посилального типу є посилання на якесь значення. 
 *     Назва об'єкту це свого роду шлях до "комірки" з властивостями та їх значеннями.
 */

"use strict"

function createNewUser() {

    let firstName = prompt("Name?");
    let lastName = prompt("LastName?");

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
    };

    return newUser;
};

let userNew = createNewUser();

console.log(userNew.getLogin());
