/*
1. Цикли у програмуванні портібні для виконання багатократного 
повторювання кусочку коду, від певного значення що є початком 
і до іншого, яке є кінцем.

2. У минулому ДЗ-2 я використовувала do while для того щоб 
перебрати перевірки для отримання від юзера саме числа в одному 
випадку і саме рядка в іншому.

3. Явне перетворення типів це коли ми використовуємо функції 
для примусової зміни типу даних коли це потрібно.

Не явне перетворення відбувається завдяки автоматичним діям в js,
наприклад впливу операторів на операнди, або функції alert яка 
автоматично переводить вміст повідомлення в string навіть якщо 
там був number. 
*/

let x = +prompt("Enter a number");

for (let i = 0; i <= x; i++) {

    if (i % 5 === 0) {
        console.log(i);
    } 

    if (x <= 4) {
        console.log("Sorry, no numbers");
    }
}