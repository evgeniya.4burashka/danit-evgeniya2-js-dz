"use strict"

let input1 = document.querySelector(".input-1");
let input2 = document.querySelector(".input-2");
let iconPassword1 = document.querySelector(".icon-password-1");
let iconPassword2 = document.querySelector(".icon-password-2");

function showPassword() {
    iconPassword1.addEventListener("click", () => {
        if (input1.getAttribute("type") == "password") {
            input1.setAttribute("type", "text");

            iconPassword1.classList.remove("fa-eye");
            iconPassword1.classList.add("fa-eye-slash");
        } else {
            input1.setAttribute("type", "password");

            iconPassword1.classList.remove("fa-eye-slash");
            iconPassword1.classList.add("fa-eye");
        };
    });

    iconPassword2.addEventListener("click", () => {
        if (input2.getAttribute("type") == "password") {
            input2.setAttribute("type", "text");

            iconPassword2.classList.remove("fa-eye");
            iconPassword2.classList.add("fa-eye-slash");
        } else {
            input2.setAttribute("type", "password");

            iconPassword2.classList.remove("fa-eye-slash");
            iconPassword2.classList.add("fa-eye");
        };
    });
};

showPassword();

let button = document.querySelector(".btn");
let valueInput1 = document.getElementById("password1");
let valueInput2 = document.getElementById("password2");
let label = document.getElementById("label");

button.addEventListener("click", (e) => {
    if (valueInput1.value === valueInput2.value) {
        e.preventDefault();
        alert("You are welcome");

    } else {
        label.insertAdjacentHTML("beforeend", `<p style="color:red; font-size:14px;">Потрібно ввести однакові значення</p>`);
        setTimeout(function () {
            document.querySelector("p").outerHTML = "";
        }, 3000);
    }
})