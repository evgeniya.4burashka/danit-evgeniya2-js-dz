/*
1. Функції дозволяють робити однакові дії багато разів без повторення коду. 
2. Аргумент передається у виклик функції для того щоб його використати у цій функції. 
   Для обчислення, або зміни значення змінної... 🤷‍♀️ 
3. return використовується у функціях повернення даних після виконання її роботи.
   Якщо функція має обробити якісь дані і потім повернути їх, то для повернення 
   даних необхідний оператор return, він зупинить функцію та надасть результат.
*/

function calculator() {
    let a = +prompt("enter the first number");
    let b = +prompt("enter the second number");
    let mathOperation = prompt("enter the calculation operator", ["+ - * /"]);
    switch (mathOperation) {
        case "+": console.log(a + b);
            break;
    
        case "-": console.log(a - b);
            break;
    
        case "*": console.log(a * b);
            break;
    
            case "/": console.log(a / b);
            break;
    }
    }
    calculator();