"use strict"
/* main */
/* Service */
const tabs = document.querySelector(".seo-ready-list-tabs");
const seoReadyBlock = document.querySelectorAll(".seo-ready-block");
const seoBlock = document.querySelectorAll(".seo-block");

const seoReadyPhoto = document.querySelector(".seo-ready-photo-text-1");

function none() {
    seoBlock.forEach(elem => {
        elem.style.display = "none";
    });
};

none();
seoReadyPhoto.style.display = "flex";


tabs.addEventListener("click", function (event) {
    seoReadyBlock.forEach(tab => {
        tab.classList.remove("seo-ready-block-active");
    });

    event.target.classList.add("seo-ready-block-active");

    seoBlock.forEach(seoBlockItem => {
        let textAttribute = seoBlockItem.getAttribute("data-name");

        if (`${textAttribute}` === event.target.dataset.name) {
            none();
            seoBlockItem.style.display = "flex";
        };
    });
});
//-------------------------------------------------------------------------------------
// rectangle-amazing-work-----
// tabs---photo
let amazingTabs = document.querySelectorAll(".amazing-work-nav-tab");
let tabOne = document.querySelector(".amazing-tab-one");
let tabRest = document.querySelectorAll(".amazing-tab-rest");

let imgWrap = document.querySelectorAll(".amazing-work-img-wrap");

function removeTab() {
    amazingTabs.forEach(tab => {
        tab.classList.remove("amazing-tab-active");
    });
};

function removeTabRest() {
    tabRest.forEach(tabR => {
        tabR.classList.remove("amazing-tab-active");
    });
};

function removeImg() {
    imgWrap.forEach(img => {
        img.classList.remove("amazing-img-active");
    });
};


tabOne.addEventListener("click", function () {
    removeTabRest();
    tabOne.classList.add("amazing-tab-active");

    imgWrap.forEach(img => {
        img.classList.add("amazing-img-active");
    });
});


tabRest.forEach(tabR => {
    tabR.addEventListener("click", function () {
        tabOne.classList.remove("amazing-tab-active");
        removeImg();
        tabR.classList.add("amazing-tab-active");

        let atrTabs = tabR.getAttribute("data-number");

        imgWrap.forEach(img => {
            let atrImg = img.getAttribute("data-number");

            if (`${atrTabs}` === `${atrImg}`) {
                removeTab();

                tabR.classList.add("amazing-tab-active");
                img.classList.add("amazing-img-active");
            };
        });
    });
});


// 12 photo---remove button
const amazingButton = document.querySelector(".amazing-button");
const removeActive = document.querySelectorAll(".remove-for-active");
let btnRemove = document.getElementById("btn-remove");

function addImg() {
    imgWrap.forEach(img => {
        img.classList.add("amazing-img-active");
    });
};

function addButton() {
    btnRemove.style.display = "flex";
};

removeActive.forEach(itemRemove => {
    amazingButton.addEventListener("click", function () {
        addImg();
        itemRemove.classList.remove("remove-for-active");
        btnRemove.style.display = "none";
    });
});
//------------------------------------------------------------------------------------
// about-theHam-img

let theHamContent = document.querySelectorAll(".about-theHam-content");
let theHamImg = document.querySelectorAll(".theHam-img-item");

const buttonLeft = document.querySelector(".next-button-wrap-left");
const buttonRight = document.querySelector(".next-button-wrap-right");


function equation(imgElem) {
    theHamContent.forEach((elem) => {

        let attribute = elem.getAttribute("data-index");
        elem.classList.remove("about-theHam-content-active");

        let attributeImg = imgElem.getAttribute("data-index");

        if (`${attribute}` === `${attributeImg}`) {
            elem.classList.add("about-theHam-content-active");
        };
    });
};


buttonRight.addEventListener("click", function () {

    let peopleActiveMini = document.querySelector(".people-active-mini");
    let nextSibling = peopleActiveMini.nextElementSibling;

    theHamImg.forEach((imgItem) => {

        if (nextSibling === null) {
            event.stopPropagation();
        } else {
            imgItem.classList.remove("people-active-mini");
            nextSibling.classList.add("people-active-mini");
            equation(nextSibling);
        };
    });
});


buttonLeft.addEventListener("click", function () {
    let peopleActiveMini = document.querySelector(".people-active-mini");
    let previousElementSibling = peopleActiveMini.previousElementSibling;

    theHamImg.forEach((imgItemM) => {

        if (previousElementSibling === null) {
            event.stopPropagation();
        } else {
            imgItemM.classList.remove("people-active-mini");
            previousElementSibling.classList.add("people-active-mini");
            equation(previousElementSibling);
        };
    });
});