"use strict"
let btn = document.querySelector(".change");
let span = document.querySelector("span");

let body = document.querySelector("body");
let headerMenuNav = document.querySelector(".header-menu-nav");
let footer = document.querySelector(".footer");

let pageColor = localStorage.getItem("data-name");

if (pageColor === "blue") {
    blue();
} else {
    white();
};

function blue() {
    span.removeAttribute("data-name");
    span.setAttribute("data-name", "blue");

    span.style.color = "#184e7a";
    body.style.backgroundColor = "#9bb8cc95";
    headerMenuNav.style.backgroundColor = "#184e7a";
    footer.style.backgroundColor = "#184e7a";
}

function white() {
    span.removeAttribute("data-name");
    span.setAttribute("data-name", "white");

    span.style.color = "#4BCAFF";
    body.style.backgroundColor = "#fff";
    headerMenuNav.style.backgroundColor = "#35444F";
    footer.style.backgroundColor = "rgba(99, 105, 110, 0.48)";
}

btn.addEventListener("click", function () {
    if (span.dataset.name === "white") {
        blue();
        pageColor = "blue";
        localStorage.setItem('data-name', pageColor);
    } else if (span.dataset.name === "blue") {
        white();
        pageColor = "white";
        localStorage.setItem('data-name', pageColor);
    }
});
