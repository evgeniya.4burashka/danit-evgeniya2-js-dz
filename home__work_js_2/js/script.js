// 1. У JS є 8 типів даних, це  number, bigint, string, boolean, null, 
// undefined, object, symbol. 

// 2. Різниця у тому що не суворе порівняння допускає перетворення типів
//  при порівнянні рівності, а суворе забороняє.

// 3. Це спеціальні символи в JS, за допомогою яких ми можемо виконувати 
// різноманітні операції над операндами. 

let name;
do {
    name = prompt("name?");
} while (name === "" || name === null || !isNaN(name));

let age; 
do {
    age = +prompt("age?");
} while (age === "" || age === null || isNaN(age));

if (age < 18 ) {
    alert("You are not allowed to visit this website"); 
} else if ( age <= 22 ) {
    answer = confirm("Are you sure you want to continue?");
    if (answer) {
        alert(`Welcome, ${name}`);  
    } else {
        alert("You are not allowed to visit this website"); 
    }
} else {
    alert(`Welcome, ${name}`);
}