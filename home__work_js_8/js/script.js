// 1. DOM - це об'єктна модель документа HTML. Він створюється браузером 
// для більш швидкоі та комфортноі роботи з ним.

// 2. innerHTML виводить у консоль весть текст елемента разом з тегами, а innerText -
// суто текст без тегів.

// 3. querySelector.

"use strict"

let elements = document.querySelectorAll('p');
console.log(elements);

elements.forEach(function (elements) {
    elements.style.background = "#ff0000";
});



let getOptionsList = document.getElementById("optionsList");
console.log(getOptionsList);

let getParentEl = getOptionsList.parentElement;
console.log(getParentEl);

let childrenElem = getOptionsList.childNodes;
console.log(childrenElem);

for(let node of childrenElem){
    console.log("Назва", node.nodeName, "тип", node.nodeType);
};



let testP = document.getElementById("testParagraph");
testP.innerHTML = "This is a paragraph";
console.log(testP);



let mainHeader = document.querySelector(".main-header");
console.log(mainHeader);

let mainHeaderItems = mainHeader.children;

for(let item of mainHeaderItems){
    item.classList.add("nav-item");
};



let getClassSectionTitle = document.querySelectorAll(".section-title");

getClassSectionTitle.forEach(function(getClassSectionTitle){
    getClassSectionTitle.classList.remove("section-title");
});

console.log(getClassSectionTitle);


