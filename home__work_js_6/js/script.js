/* 1. Екранування - це використання зворотнього слешу у рядках для того 
      щоб можна було використовувати спеціальні символи як звичайні.

   2. Function Expression
      Function Declaration
      Named Function Expression

   3. Hoisting - це коли змінна або функція оголошується вище до виконання коду,
      таке свого роду "підняття", але значення змінних залишаються на своїх місцях.
      Однією з переваг підйому є те, що він дозволяє нам використовувати функції 
      перед їх оголошенням у коді. 
 */

"use strict"

function createNewUser() {

    let firstName = prompt("Name?");
    let lastName = prompt("LastName?");

    let birthday = prompt("Write your date of birth...", ["dd.mm.yyyy"]);
    let birthdayArr = birthday.split(".");
    let birthDay = parseInt(birthdayArr[0]);
    let birthMonth = parseInt(birthdayArr[1]);
    let birthYear = parseInt(birthdayArr[2]);

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function () {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },

        getAge: function () {
            let today = new Date();
            let currentYear = today.getFullYear();
            let currentMonth = today.getMonth() + 1;
            let currentDay = today.getDate();

            let age = currentYear - birthYear;
            if (currentMonth < birthMonth || (currentMonth === birthMonth && currentDay < birthDay)) {
                age--;
            };

            return age;
        },

        getPassword: function () {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${birthYear}`;
        }
    };

    return newUser;
};

let userNew = createNewUser();

console.log(userNew.getLogin());

console.log(userNew.getPassword());

console.log(userNew.getAge());

