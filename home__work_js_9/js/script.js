// 1. Наприклад за допомогою insertAdjacentHTML. Потрібно прив'язати його до 
//    існуючого елементу в html, знайденого наприклад через querySelector. Далі 
//    вписати параметри: 1-позицію елементу який створюємо і 2-тег з текстом. 

// 2. "...1-позицію елементу який створюємо". Варіанти параметру: 
//    "beforebegin", "afterbegin", "beforeend", "afterend".

// 3. Знайти його спочатку через querySelector, після, через присвоєну до 
//    нього змінну, прописати через крапку remove().


"use strict"

let root = document.body.querySelector("#root");

let array = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];

function transformArrayList(arr, elem) {
   
   for(let item of arr){
    item += elem.insertAdjacentHTML("beforeend", `<li>${item}</li>`)
   };

};

transformArrayList(array, root);

