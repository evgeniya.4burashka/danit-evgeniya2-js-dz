"use strict"
let tabsTitle = document.querySelectorAll(".tabs-title");

let text = document.querySelectorAll(".tabs-content li");

let tabs = document.querySelector(".tabs");

let akali = document.querySelector(".akali");

 
function none() {
    text.forEach(elem => {
        elem.style.display = "none";
    });
};

none();
akali.style.display = "block";


tabs.addEventListener("click", function (event) {

    tabsTitle.forEach(tab => {
        tab.classList.remove("active");
    });

    event.target.classList.add("active");

    text.forEach(textItem => {
        let textAttribute = textItem.getAttribute("data-name");
    
        if (`${textAttribute}` === event.target.dataset.name) {
            none();
            textItem.style.display = "block";
        };
    });
});