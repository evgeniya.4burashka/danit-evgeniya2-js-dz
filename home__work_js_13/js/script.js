// 1. setTimeout() виконується раз через заданий час, а setInterval() 
// виконується постійно з заданою періодичністю.

// 2. Спрацює миттєво. На мою думку тому що чітко вказаний час, а 
// тобто через 0 сек.

// 3. На мою думку тому що її постійна дія буде перевантажувати робрту 
// браузера, через що збільшується час обробки коду і взаємодії з користувачем.

"use strict"
let imageShow = document.querySelectorAll(".images-wrapper img");

let red = document.querySelector(".red");
let green = document.querySelector(".green");

let current = 0;

function slider() {
    for (let i = 0; i < imageShow.length; i++) {
        imageShow[i].classList.add("opacity0");
    }
    imageShow[current].classList.remove("opacity0");

    if (current + 1 === imageShow.length) {
        current = 0;
    } else {
        current++;
    };
};

let stopInterval;

function goInterval() {
    stopInterval = setInterval(() => {
        slider();

        setTimeout(() => {
            red.style.display = "block";
        }, 12000);

    }, 3000);
};

goInterval();

red.addEventListener("click", function (event) {
    if (event.target === red) {
        clearInterval(stopInterval);

        setTimeout(() => {
            green.style.display = "block";
        }, 1000);
    };
});

green.addEventListener("click", function(ev) {
    goInterval();

    green.style.display = "none";
});