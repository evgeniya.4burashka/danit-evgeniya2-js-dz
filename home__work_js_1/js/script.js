// 1. Оголосити змінну в js можна за допомогою ключових слів var, const i let.

// 2. Окрім 'ок' та 'скасування' як в confirm, у prompt є ще й рядок для введення 
// відповіді користувачем на запитання яке з'являеться у модальному вікні.

// 3. Не явне перетворення це автоматичне перетворення типів у процесі обробки
// браузером коду завдяки впливу різних вакторів у мові програмування JavaScript.
// Наприклад: 
// console.log(5 + "5"); // поверне 55, число перетвориться на рядок.

let admin;
let name;
name = "Zhenya";
admin = name;
console.log(admin);

let days = 3;
let seconds = 60 * 60 * 24 * days;
console.log(seconds);

const number = prompt("write the number...");
console.log(number);


